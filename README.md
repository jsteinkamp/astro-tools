# Astro-Tools

This repository contains several scripts:

* [sun.py](bin/sun.py): Calculating sunrise/-set, dusk dawn and returning it as html-table.
* [moon.py](bin/moon.py): Calculation moonrise/-set, culmination and phase for the next 14 days.
* [skymap.py](bin/skymap.py): plot a map of some atronomical object

The above 3 scripts contain my hardcoded location settings and results can be seen on my webpage ([Astronomical targets](https://www.j4s8.de/post/2021-10-10-astronomical-targets/) and [Skymap](https://www.j4s8.de/post/2021-11-01-skymap/))

* siril-multisession.sh: run siril-cli to process lights taken over several night with
                         individual darks, flats and/or biases or all in common.

* pixelsize-relations.pl: make plotly 3D graphics about
                          aperture - resolving capacity - pixelsize

Additionally I've included a [Jupiter Notebook](Skymap.ipynb) including the output image of skymap.py.

## Skymap

The skymap.py script reads two files, a simple csv file of individual objects
and plots them according to the given coordinates on a polar plot. Additional to
the right ascension (RA) the date of maximum culmination at midnight is given as
second radial axis. The objects are distinguished by color and symbol based on
their type. And the apparent magnitide (amag) is used to scale those symbols in 
size.

The other file is a JSON file of constellations. Below is an empty template.

The ecplitic and the mentioned date axis are not 100% correct, since the earth 
rotates not always at the same speed and on my plot the RA 0h corresponds to 
19. September but should be the 23. September.


### Empty template for constelations
```
    {
        "name": "",
        "stars":
        {
            "id": [],
			"name": [],
            "RA": [],
            "dec": [],
            "amag":[]
        },
        "connections":
        {
            "from": [],
            "to":[]
        }
    }
```
