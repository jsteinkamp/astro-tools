#!/usr/bin/python3

## apt install python3-ephem
import ephem

import re
from datetime import datetime, timedelta
from pytz import timezone
from math import pi,ceil,floor

## in m
# AU = 1.495978707e11
## in km
AU = 1.495978707e8

# origin = datetime.strptime("1899 December 31 12:00 UTC", '%Y %B %d %H:%M %Z')

def rad2deg(x):
  return(180.0 * x / pi)

def deg2rad(x):
  return(pi * x / 180.0)

def utc2localtime(dt, tz):
  dt = dt.replace(tzinfo=timezone('UTC'))
  return(dt.astimezone(tz))

## TODO: * make date optional (use the one from the observer)
##       * local or UTC (defaults UTC)
def rise_transit_set(observer, target, date, tz):
  deltatz = date.replace(tzinfo=timezone("UTC")) - date
  date=date - deltatz
  date = date.replace(tzinfo=None)
  observer.date = date
  target.compute(observer)
  rising  = utc2localtime(observer.next_rising(target).datetime(), tz)
  setting = utc2localtime(observer.next_setting(target).datetime(), tz)
  transit = utc2localtime(observer.next_transit(target).datetime(), tz)
  transit_angle = target.alt
  phase   = "%0.0f" % target.phase

  transit_angle = rad2deg(transit_angle)
  distance = target.earth_distance * AU
  return({'rising'  : rising,
          'transit' : transit,
          'setting' : setting,
          'transit_angle' : transit_angle,
          'phase' : phase,
          'distance': distance})

def next_moon_phases(date, tz='UTC'):
  next_new  = ephem.next_new_moon(date).datetime()
  next_first_quarter = ephem.next_first_quarter_moon(date).datetime()
  next_full = ephem.next_full_moon(date).datetime()
  next_last_quarter = ephem.next_last_quarter_moon(date).datetime()

  next_new = utc2localtime(next_new, tz)
  next_first_quarter = utc2localtime(next_first_quarter, tz)
  next_full = utc2localtime(next_full, tz)
  next_last_quarter = utc2localtime(next_last_quarter, tz)
  
  moon_phases = {}
  moon_phases['new'] = next_new
  moon_phases['first_quarter'] = next_first_quarter
  moon_phases['full'] = next_full
  moon_phases['last_quarter'] = next_last_quarter
  return(moon_phases)

tz       = timezone('Europe/Berlin')
date     = datetime.now(tz) - timedelta(days=1)
# date     = datetime.strptime("1899 December 31 12:00 UTC", '%Y %B %d %H:%M %Z')
# date     = datetime.strptime("2021 October 26 12:00 UTC", '%Y %B %d %H:%M %Z')
date = date.replace(minute=0, second=0, microsecond=0, hour=0)

location      = ephem.Observer()
## geht auch als 'dd:mm:ss'
location.lon  = '8.2764' 
location.lat  = '50.0065'
location.elev = 80.0
location.date = date

moon = ephem.Moon()

next_mp = next_moon_phases(date, tz)

print("<table>")
print("<tr><th>Date</th><th colspan=3>rise(&#xa71b;), culmin.(&#x293c;), set(&#xa71c;)</th><th>frac.</th><th>dist.</th></tr>")

for i in range(31):
  x = rise_transit_set(location.copy(), moon.copy(), date, tz)
  print("<tr>")
  print("<td>%s</td>" % date.strftime("%a %d.%m."))

  if (x['rising'] < x['transit'] and x['transit'] < x['setting']):
    print("<td>&#xa71b;%s</td>" % x['rising'].strftime("%H:%M"))
    print("<td>")
    if date.day == x['transit'].day:
      print("&#x293c;%s" % x['transit'].strftime("%H:%M"))
    print("</td><td>")
    if date.day == x['setting'].day:
      print("&#xa71c;%s" % x['setting'].strftime("%H:%M"))
    print("</td>")
  
  elif (x['transit'] < x['setting'] and x['setting'] < x['rising']):
    print("<td>&#x293c;%s</td>" % x['transit'].strftime("%H:%M"))
    print("<td>")
    if date.day == x['setting'].day:
      print("&#xa71c;%s" % x['setting'].strftime("%H:%M"))
    print("</td><td>")
    if date.day == x['rising'].day:
      print("&#xa71b;%s" % x['rising'].strftime("%H:%M"))
    print("</td>")
  elif (x['setting'] < x['rising'] and x['rising'] < x['transit']):
    print("<td>&#xa71c;%s</td>" % x['setting'].strftime("%H:%M"))
    print("<td>")
    if date.day == x['rising'].day:
      print("&#xa71b;%s" % x['rising'].strftime("%H:%M"))
    print("</td><td>")
    if date.day == x['transit'].day:
      print("&#x293c;%s" % x['transit'].strftime("%H:%M"))
    print("</td>")
  else:
    print("JS_DEBUG SHIT in else")

  if next_mp['new'].day == date.day and next_mp['new'].month == date.month:
    phase    = "&#x1f311;"
  elif next_mp['first_quarter'].day == date.day and next_mp['first_quarter'].month == date.month:
    phase    = "&#x1f313;"
  elif next_mp['full'].day == date.day and next_mp['full'].month == date.month:
    phase    = "&#x1f315;"
  elif next_mp['last_quarter'].day == date.day and next_mp['last_quarter'].month == date.month:
    phase    = "&#x1f317;"
  else:
    phase    = str(x['phase']) + "%"
  print("<td>%s</td>" % phase)
  distance    = ("%1.0f" % (x['distance']))
  print("<td>%s</td></tr>" % distance)
  date = date + timedelta(days=1)

print("</table>")



#date     = datetime.strptime("2021 November 22 22:45:46", '%Y %B %d %H:%M:%S')
#date=date.replace(tzinfo=timezone("Europe/Berlin"))
#print(date)
#x = rise_transit_set(location.copy(), moon.copy(), date, tz)
#print(x)
