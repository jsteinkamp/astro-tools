#!/usr/bin/perl

## https://www.sterngucker.de/astrofotografie/sampling/
## https://www.andreas-schupies.de/astronomie/4-das-aufl%C3%B6sungsverm%C3%B6gen-von-teleskopen/
##
## https://de.wikipedia.org/wiki/Bildwinkel
## https://forum.astronomie.de/threads/gesichtsfeld-beim-fotografieren.218973/
##
## http://www.syracuse-astro.org/2021/08/23/comparing-images-of-m13-between-a-10-sct-and-a-5-mak/

## Bei einem durchschnittlichem Seeing von ~3" in Dtschl. spielt das Aperture-bedingte (bessere)
## Auflösungsvermögen einer Öffnung > 138mm für DSO keine Rolle mehr. Nur bei kurz belichteten
## Mond-/Planeten-Fotos

use strict;
use warnings;
use POSIX;         ## for ceil()
use JSON::MaybeXS; ## for JSON()->true/false;
use Math::Trig;    ## for pi()

## For 3x3 pixels to get a "round" star multiply the pixelsize by 3
## pixelsize = sensorwidth in m / pixel count
my $pxs = 0.0173 / 4608; # 16MP @ MFT
#my $pxs = 0.0173 / 5200; # 20MP @ MFT
#my $pxs = 0.0074 / 3096; # veLOX 178 C (3096x2080 @ 7.4x4.9)

sub range($$$) {
    my $start = shift;
    my $end   = shift;
    my $step  = shift;

    $step = -1 * $step if ($start > $end && $step > 0);
    my $n = ceil(($end - $start) / $step);
    my @a = (0..$n);
    foreach my $x (@a) { $x = $x * $step + $start; };
    # @a = map { $_ * $step + $start } @a;
    return(@a);
}

sub transpose($) {
    my $ref = shift;
    my @m = @$ref;
    my @t = ();
    for (my $i = 0; $i <= $#m; $i++) {
	for (my $j = 0; $j <= $#{$m[$i]}; $j++) {
	    push(@{$t[$j]}, $m[$i][$j]);
	}
    }
    return(\@t);
}

sub array2matrix($$) {
    my $aref = shift;
    my $n = shift;
    my @a = @$aref;
    my @m = ();
    for (my $i=0; $i <= $n; $i++){
	push(@m, \@a);
    }
    return(\@m);
}

## Define aperture diameter in m
my @d = range(0.07, 0.154, 0.002);

## Calculate minimum distance of distinguishable stars
## in seconds for the given diameters
## 3600:   degree to seconds
## 180/pi: Rad to Degree
## 550e-9: aprox. center of visible light (380-750nm)
my @z = map { 1.22 * 3600 * 180 / pi() * 550e-9 / $_ } @d;

## Calculate focal length from above aperture dependent resolution,
## so that the above calculated min. distance corresponds to one pixel
my @f = map { $pxs / 2 / tan($_ * pi() / 180 / 3600 / 2) } @z;

## Turn those array into matices for 3D-plot
my @zd = @{array2matrix(\@z, $#f)};
my @zf = @{array2matrix(\@z, $#d)};

@zf = @{transpose(\@zf)};

## Convert focal length and aperture from m to mm for nicer axis-label
@d = map { $_ * 1000 } @d;
@f = map { $_ * 1000 } @f;

## Create 2D-plotly file for the pixelsize dependency on aperture
my %plotly;
$plotly{'config'} = {responsive => JSON()->true};
$plotly{'layout'} = { title => 'Aperture - Pixel size',
		      showlegend => JSON()->false,
		      height => 500,
		      width => 500,
		      margin => {t => 40, r => 50, b => 90, l => 65 },
		      xaxis => {showgrid => JSON()->true,
				title => 'Aperture [mm]'},
		      yaxis => {showgrid => JSON()->true,
				title => 'Resolution [sec]'},
};
$plotly{'data'} = [{type => 'scatter',
		    opacity => 0.8,
		    showscale => JSON()->false,
		    x => \@d,
		    y => \@z,
		    xaxis => 'x',
		    yaxis => 'y'}];

open(FH, '>', "pixelsize-aperture.json") or die("Cannont open file: $!\n");
print FH encode_json(\%plotly);
close(FH);

## Create the 3D-plotly file for the pixelsize (z-axis) dependency on
## aperture (x-axis)
## focal length (y-axis)
## with a single pixelsize ($pxs)
%plotly = ();
$plotly{'config'} = {responsive => JSON()->true};
$plotly{'layout'} = { title => 'Aperture - Focal length - Pixel size',
		      showlegend => JSON()->false,
		      height => 500,
		      width => 500,
		      margin => {t => 50, r => 50, b => 90, l => 65 },
		      scene => {
			  xaxis => {showgrid => JSON()->true,
				title => 'Aperture [mm]'},
			  yaxis => {showgrid => JSON()->true,
				    title => 'Focal length [mm]'},
			  zaxis => {showgrid => JSON()->true,
				    title => 'Resolution [sec]'},
		      }
};
$plotly{'data'} = [{type => 'surface',
		    opacity => 0.8,
		    showscale => JSON()->false,
		    x => \@d,
		    y => \@f,
		    z => \@zd,
		    xaxis => 'x',
		    yaxis => 'y'},
		   {type => 'surface',
		    opacity => 0.8,
		    showscale => JSON()->false,
		    x => \@d,
		    y => \@f,
		    z => \@zf,
		    xaxis => 'x',
		    yaxis => 'y'},
		   {type => 'scatter3d',
		    mode => "lines",
		    opacity => 1,
		    x => \@d,
		    y => \@f,
		    z => \@z,
		    line => {width => 6,
			     color => 'black'
		    },
		   }];

open(FH, '>', "pixelsize-aperture-focallength.json") or die("Cannont open file: $!\n");
print FH encode_json(\%plotly);
close(FH);

## Create 2D-plotly file for the resolution dependency on focal length and
## 3 pixel sizes (2,4, 3.7 µm)
@f = range(300, 2000, 25);
%plotly = ();
$plotly{'config'} = {responsive => JSON()->true};
$plotly{'layout'} = { title => 'Focal length - Pixel size',
		      showlegend => JSON()->true,
		      height => 500,
		      width => 500,
		      margin => {t => 40, r => 50, b => 90, l => 65 },
		      xaxis => {showgrid => JSON()->true,
				title => 'Focal length [mm]'},
		      yaxis => {showgrid => JSON()->true,
				title => 'Resolution [sec]'},
};
$plotly{'data'} = ();
my $pxs_tmp = 0;
for $pxs_tmp (2.4, 3.7) {
    my @y = map { 2 * 3600 * 180 / pi() * atan($pxs_tmp / 2 / $_ *1.e-3 ) } @f;
    my $dat = {type => 'scatter',
	       opacity => 0.8,
	       x => \@f,
	       y => \@y,
	       name => "$pxs_tmp &#181;m",
	       xaxis => 'x',
	       yaxis => 'y'};
    
    push(@{$plotly{'data'}}, $dat );
}

open(FH, '>', "pixelsize-focallength.json") or die("Cannont open file: $!\n");
print FH encode_json(\%plotly);
close(FH);

#$pxs = 5.4e-6;
#print("$pxs : ");
#my $x = 1.5;
#print($pxs * pi() * $x /2/tan(pi() * $x / 180/2/3600)/1.22/3600/180/550e-9);
#print("\n");
