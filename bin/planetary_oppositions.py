#!/usr/bin/python3


## besser: https://rhodesmill.org/skyfield/examples.html als zusätzlich ephem zu verwenden


import ephem
from datetime import datetime
from pytz import timezone
from skyfield import api
from skyfield import almanac
from math import pi, floor

def rad2time(r, base=24, precision=3):
  t =  base / 2 * r / pi
  h = floor(t)
  m = floor((t - h) * 60)
  s = (t - h - m / 60) * 3600
  fmt = "%%02i:%%02i:%%0%i.%if" % (precision+3, precision)
  return(fmt % (h,m,s))

def utc2localtime(dt, tz):
  dt = dt.replace(tzinfo=timezone('UTC'))
  return(dt.astimezone(tz))

## set local timezone
tz       = timezone('Europe/Berlin')

## Setup an observer on earth
observer = ephem.Observer()
observer.lon  = '8.2764' 
observer.lat  = '50.0065'
observer.elev = 96.0

## Download
ts = api.load.timescale()
eph = api.load('de421.bsp')

## Let's choose the current year until 2050
t0 = ts.utc(2022, 1, 1)
t1 = ts.utc(2025, 1, 1)

## Loop over the most prominent outer planets 
for target in (ephem.Mars(), ephem.Jupiter(), ephem.Saturn()):
## Now choose the date where y==1
## -> read the documentation, what 0 and 1 stand for respectively.
##    Huge difference inner and outer planets!!!
  print("### %s ###" % target.name)
  print()
  ## Calculate the dates for the target
  f = almanac.oppositions_conjunctions(eph, eph[("%s barycenter" %target.name.lower())])
  t, y = almanac.find_discrete(t0, t1, f)

  for date in [t[i] for i in range(len(y)) if y[i]==1]:
    date = date.utc_datetime()
    date = date.replace(microsecond=0)
    print("  Exact Opposition: %s" % utc2localtime(date,tz))
    ## Reset to previous midnight
    midnight = date.replace(minute=0, second=0, microsecond=0, hour=0)

    ## Set the date of the observation
    observer.date = midnight

    ## Compute 
    target.compute(observer)

    ## get the exact next transit time
    trans_time = observer.next_transit(target).datetime()
    trans_time = trans_time.replace(microsecond=0)
    print("  Meridian transit: %s" % utc2localtime(trans_time, tz))

    observer.date = observer.next_transit(target).datetime()
    target.compute(observer)

    ## Alt/Az RA/Dec is returned in radians
    print("    Altitude: %.2f°" % (target.alt.real * 180 / pi))
    print("    Azumit: %.2f°" % (target.az.real * 180 / pi))
    print("    RA: %s" % rad2time(target.ra.real))
    print("    Dec: %.2f°" % (target.dec.real * 180 / pi))
    print("    Distance: %.3f AU" % (target.earth_distance.real))
    print("    Mag: %.2f" % (target.mag.real))
    print("    size: %.1f\"" % (target.size.real))
    print()
