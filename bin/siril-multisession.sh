#!/bin/bash

IFS=$'\t\n'
set -euo pipefail

usage() {
cat <<EOF
$0 [-h] [-g] [-w <working directory>] [-e ext] [-d darks] -b [biases] -f [flats] session ...

Wrapper around siril-cli, to combine the pictures of several sessions (night) with individual darks, flats and/or biase

Options:
 -h : print this help
 -g : background reduction on each input image with polynom of 1st order, 20 samples, tolerance 1.0
 -k : keep the 'process', default is to delete it

with Parameters
 -w <working directory> : define the working directory, where the temporary directory 'process' is created and the final 'result.fit' is stored, default current working directory
 -e <ext> : set file extension, default 'fit'
 -b <biases directory> : use a common directory for biases used for all sessions
 -d <darks directory> : use a common directory for darks used for all sessions
 -f <flats directory> : use a common directory for flats used for all sessions

Finally the session directories should be given, either absolut or releative to the <working directory>. Here at least the 'lights' folder should be present, holding the pictures.

If no 'biases', 'darks' and/or 'flats' directories are given, it is assumed that they exist in the respective session directory.

EOF
}

### background gradient extration
bkg=0
###file extension to use
ext=fit
## Working directory is current working directory.
## Could be somewhere else, but not tested
base=$PWD

darks=""
flats=""
biases=""

keep=0

## directory with images, at least 'lights'
## if a relative path is given,
## it is relative to the base directory
## Those RAW source images could be somewhere else
#session_src=(${base}/20220204 ${base}/20220303)
session_src=( )

while getopts "hkgw:e:d:b:f:" o; do
    case "${o}" in
      h)
	usage
	exit
	;;
      k)
	keep=1
	;;
      g)
	bkg=1
	;;
      w)
	base=${OPTARG}
	if [[ ! "$base" =~ ^/ ]]; then
	  $base=$PWD/$base
	fi
	;;
      e)
	ext=${OPTARG}
	;;
      d)
	darks=${OPTARG}
	;;
      b)
	biases=${OPTARG}
	;;
      f)
	flats=${OPTARG}
	;;
    esac
done
version=$(siril --version | awk '{print $2}')
count=1
shift $((OPTIND-1))

session_src=( $@ )

echo keep    $keep
echo bkg     $bkg
echo wd      $base
echo ext     $ext
echo darks   $darks
echo biases  $biases
echo flats   $flats
echo ${session_src[@]}

#if [ ! -d ${base}/process ]; then
#  mkdir -p ${base}/process
#fi

if [[ -z "$biases" && -n "$flats" ]]; then
  echo "ERROR: It makes no sense, to have common flats and individual biases."
  exit 1
fi


if [[ -n "$biases" ]]; then
  if [ ! -d $biases ]; then
    echo "Error: biases directory '$biases' does not exist" >&2
    exit 1
  fi
  siril-cli -i ~/.config/siril/siril.cfg -s - <<ENDSIRIL
requires $version
setext $ext

cd $biases
convert bias -out=${base}/process
cd ${base}/process
stack bias rej 3 3 -nonorm
ENDSIRIL
fi

if [[ -n "$darks" ]]; then
  if [ ! -d $darks ]; then
    echo "Error: darks directory '$darks' does not exist" >&2
    exit 1
  fi
  siril-cli -i ~/.config/siril/siril.cfg -s - <<ENDSIRIL
requires $version
setext $ext

cd $darks
convert dark -out=${base}/process
cd $base/process
stack dark rej 3 3 -nonorm
ENDSIRIL
fi

if [[ -n "$flats" ]]; then
  if [ ! -d $flats ]; then
    echo "Error: flats directory '$flats' does not exist" >&2
    exit 1
  fi
  siril-cli -i ~/.config/siril/siril.cfg -s - <<ENDSIRIL
requires $version
setext $ext

cd $base/flats
convert flat -out=${base}/process
ENDSIRIL
fi

if [[ -n "$biases" && -n "$flats" ]]; then
  siril-cli -i ~/.config/siril/siril.cfg -s - <<ENDSIRIL
requires $version
setext $ext

cd ${base}/process
preprocess flat -bias=bias_stacked
stack pp_flat rej 3 3 -norm=mul
ENDSIRIL
fi

## Pre-Process in the subfolders (sessions, with different darks)
for d in ${session_src[*]}; do
  session=$(basename $d)
  parent_d=$(dirname $d)

  if [[ ! "$parent_d" =~ ^/ ]]; then
    parent_d=$PWD/$parent_d
  fi
  
  if [ ! -d ${base}/process/${session} ]; then
    mkdir -p ${base}/process/${session}
  fi

  if [[ -n "$biases" && -n "$flats" ]]; then
    ln -s ${base}/process/pp_flat_stacked.${ext} ${base}/process/${session}/
  elif [[ -n "$biases" && -z "$flats" ]]; then
    ln -s ${base}/process/bias_stacked.${ext} ${base}/process/${session}/
    siril-cli -i ~/.config/siril/siril.cfg -s - <<ENDSIRIL
requires $version
setext $ext

cd $parent_d/$d/flats
convert flat -out=${base}/process/${session}

cd ${base}/process/${session}
preprocess flat -bias=bias_stacked
stack pp_flat rej 3 3 -norm=mul
ENDSIRIL
  elif [[ -z "$biases" && -n "$flats" ]]; then
    echo "It makes no sense, to have common flats and individual biases."
    exit 1
  else
    siril-cli -i ~/.config/siril/siril.cfg -s - <<ENDSIRIL
requires $version
setext $ext

cd $parent_d/$d/biases
convert bias -out=${base}/process/${session}

cd $parent_d/$d/flats
convert flat -out=${base}/process/${session}

cd ${base}/process/${session}
stack bias rej 3 3 -nonorm
preprocess flat -bias=bias_stacked
stack pp_flat rej 3 3 -norm=mul
ENDSIRIL
  fi

  if [[ -n "$darks" ]]; then
    ln -s ${base}/process/dark_stacked.${ext} ${base}/process/${session}/
  else
    siril-cli -i ~/.config/siril/siril.cfg -s - <<ENDSIRIL
requires $version
setext $ext

cd $parent_d/$d/darks
convert dark -out=${base}/process/${session}
cd ${base}/process/${session}
stack dark rej 3 3 -nonorm
ENDSIRIL
  fi
  
  siril-cli -i ~/.config/siril/siril.cfg -s - <<ENDSIRIL
requires $version
setext $ext

cd $parent_d/$d/lights
convert light -start=$count -out=${base}/process/${session}

cd ${base}/process/${session}
preprocess light -dark=dark_stacked -flat=pp_flat_stacked -cfa -equalize_cfa -debayer

close
ENDSIRIL

  if [[ $bkg != 0 ]]; then
    siril-cli -i ~/.config/siril/siril.cfg -s - <<ENDSIRIL
requires $version
setext $ext

cd ${base}/process/${session}
seqsubsky pp_light_ 1 -samples=20 -tolerance=1.0 -smooth=0.5

close
ENDSIRIL
    count=$(( $count + $(ls ${base}/process/${session}/bkg_pp_light_*.${ext} | wc -l) ))
    mv ${base}/process/${session}/bkg_pp_light_*.${ext} ${base}/process/
  else
    count=$(( $count + $(ls ${base}/process/${session}/pp_light_*.${ext} | wc -l) ))
    mv ${base}/process/${session}/pp_light_*.${ext} ${base}/process/
  fi
done

## Register and stack
if [[ $bkg != 0 ]]; then
  siril-cli -i ~/.config/siril/siril.cfg -s - <<ENDSIRIL
requires $version
setext $ext
cd $base/process
register bkg_pp_light_
stack r_bkg_pp_light_ rej 3 3 -norm=addscale -output_norm -out=$base/result.${ext}
close
ENDSIRIL
else
  siril-cli -i ~/.config/siril/siril.cfg -s - <<ENDSIRIL
requires $version
setext $ext
cd $base/process
register pp_light_
stack r_pp_light_ rej 3 3 -norm=addscale -output_norm -out=$base/result.${ext}
close
ENDSIRIL

fi

if [[ $keep -eq 0 ]]; then
   rm -r ${base}/process/
fi
