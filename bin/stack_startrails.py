#!/usr/bin/python3

import os
import argparse
import rawpy as rp
import numpy as np
import tifffile
from glob import glob

## for random file name generation
import random
import string

def getImageData(path, extension='', verbose=0):
  files = sorted(glob("%s/*%s" %(path, extension))) # key=os.path.getmtime
  for f in files:
    with rp.imread(f) as raw:
      if verbose > 0:
        print("Read '%s'" %f)
      ## "no_auto_scale=True" macht es zu dunkel
      default_kwargs = dict(output_bps=16,
                            use_camera_wb=False,
                            use_auto_wb=False,
                            no_auto_bright=True,
                            user_flip=0)
      img = raw.postprocess(**default_kwargs)
      if 'data' in locals():
        data = np.append(data, np.expand_dims(img, axis=3), axis=3)
      else:
        data = np.expand_dims(img, axis=3)
  return(data)

## https://e2eml.school/convert_rgb_to_grayscale.html
def luminance(img, depth=16, perceptual=True, gamma=False):
  r = img[:,:,0].copy()
  g = img[:,:,1].copy()
  b = img[:,:,2].copy()

  if gamma == True:
    r = r / (2 ** depth - 1)
    g = g / (2 ** depth - 1)
    b = b / (2 ** depth - 1)
    r[r <= 0.04045] = r[r <= 0.04045] / 12.92
    r[r > 0.04045]  = ((r[r > 0.04045] + 0.055) / 1.055) ** 2.4
    g[g <= 0.04045] = g[g <= 0.04045] / 12.92
    g[g > 0.04045]  = ((g[g > 0.04045] + 0.055) / 1.055) ** 2.4
    b[b <= 0.04045] = b[b <= 0.04045] / 12.92
    b[b > 0.04045]  = ((b[b > 0.04045] + 0.055) / 1.055) ** 2.4
    r = r * (2 ** depth - 1)
    g = g * (2 ** depth - 1)
    b = b * (2 ** depth - 1)
    
  if perceptual == True:
    l = 0.2126 * r +  0.7152 * g +  0.0722 * b
  else:
    l = (r + g + b) / 3
  return(l)

def main():
  parser = argparse.ArgumentParser(description='Stack a bunch of startrail images')
  parser.add_argument('-v', dest='verbose', action='count', default=0, help='verbosity level')
  parser.add_argument('-g', dest='gamma', action='store_true', default=False, help='use gamma correction to find brighter pixels (default=False)')
  parser.add_argument('-l', dest='dir_lights', metavar='DIR', default='lights', help='Folder containing lights (default="lights")')
  parser.add_argument('-d', dest='dir_darks', metavar='DIR', default='darks', help='Folder containing darks (default="darks")')
  parser.add_argument('-f', dest='dir_flats', metavar='DIR', default='flats', help='Folder containing flats (default="flats")')
  parser.add_argument('-b', dest='dir_biases', metavar='DIR', default='biases', help='Folder containing biases (default="biases")')
  parser.add_argument('-e', dest='extension', metavar='EXTENSION', default='.RW2', help='Input filename extension (default=".RW2")')
  parser.add_argument('-o', dest='outfile', metavar='FILE', default='result.tif', help='Output filename (default="result.tif")')

  args = parser.parse_args()

  if args.verbose > 1:
    prefix_out=''.join(random.choices(string.ascii_uppercase + string.digits, k=8))

  images = getImageData(args.dir_biases, args.extension, args.verbose)
  bias = np.average(images, 3)
  images = getImageData(args.dir_flats, args.extension, args.verbose)
  flat = np.average(images, 3)
  images = getImageData(args.dir_darks, args.extension, args.verbose)
  dark = np.average(images, 3)

  ## Calculate the divisor for flat/bias correction
  ## was ist richtig?
  ## https://forum.astronomie.de/threads/bias-darks-und-flats-ein-ueberblick.305382/post-1570605
  ## (light - dark) / (flat - bias)

  divisor = flat - bias
  ## set pixels with higher bias values to "black"
  divisor[divisor < 0] = 65535
  divisor = divisor + 1

  lights = getImageData(args.dir_lights, args.extension, args.verbose)

  for i in range(lights.shape[3]):
    if args.verbose > 0:
      print("Process %i/%i frames" % (i+1, lights.shape[3]))
    corr = (lights[:, :, :, i] - dark ) / divisor
    corr[corr < 0] = 0
    corr_lum = np.repeat(luminance(corr, gamma=args.gamma)[:, :, np.newaxis], 3, axis=2)
    if 'final' in locals():
      final[corr_lum > final_lum] = corr[corr_lum > final_lum]
      final_lum = np.repeat(luminance(final, gamma=args.gamma)[:, :, np.newaxis], 3, axis=2)
    else:
      final = corr.copy()
      final_lum = corr_lum

    if args.verbose > 1:
      fout = "%s_%04i.tif" % (prefix_out, i)
      corr = (corr - np.min(corr)) / (np.max(corr) - np.min(corr)) * 65535
      tifffile.imsave(fout, np.array(corr, dtype="uint16"))
      print("Saved corrected image as '%s'" % fout)
      
  final = (final - np.min(final)) / (np.max(final) - np.min(final)) * 65535

  tifffile.imsave(args.outfile, np.array(final, dtype="uint16"))
  if args.verbose > 0:
    print("Saved final image as '%s'" % args.outfile)
  
if __name__ == '__main__':
  main()
