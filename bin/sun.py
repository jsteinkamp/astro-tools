#!/usr/bin/python3

## apt install python3-ephem
import ephem

import re
from datetime import datetime
from pytz import timezone
from math import pi,ceil,floor

## in m
# AU = 1.495978707e11
## in km
AU = 1.495978707e8

# origin = datetime.strptime("1899 December 31 12:00 UTC", '%Y %B %d %H:%M %Z')

def rad2deg(x):
  return(180.0 * x / pi)

def deg2rad(x):
  return(pi * x / 180.0)

def utc2localtime(dt, tz):
  dt = dt.replace(tzinfo=timezone('UTC'))
  return(dt.astimezone(tz))

## TODO: * make date optional (use the one from the observer)
##       * local or UTC (defaults UTC)
def rise_transit_set(observer, target, date):
  tz = date.tzinfo
  midnight = date.replace(minute=0, second=0, microsecond=0, hour=0)
  observer.date = midnight
  target.compute(observer)
  rising  = utc2localtime(observer.next_rising(target).datetime(), tz)
  setting = utc2localtime(observer.next_setting(target).datetime(), tz)
  transit = utc2localtime(observer.next_transit(target).datetime(), tz)
  transit_angle = target.alt

  observer.horizon = '6'
  golden_rise   = utc2localtime(observer.next_rising(sun, use_center=True).datetime(), tz)
  golden_set    = utc2localtime(observer.next_setting(sun, use_center=True).datetime(), tz)
  observer.horizon = '-4'
  blue_morning_end   = utc2localtime(observer.next_rising(sun, use_center=True).datetime(), tz)
  blue_evening_begin = utc2localtime(observer.next_setting(sun, use_center=True).datetime(), tz)
  observer.horizon = '-6'
  civil_rise         = utc2localtime(observer.next_rising(sun, use_center=True).datetime(), tz)
  civil_set          = utc2localtime(observer.next_setting(sun, use_center=True).datetime(), tz)
  observer.horizon = '-8'
  blue_morning_begin = utc2localtime(observer.next_rising(sun, use_center=True).datetime(), tz)
  blue_evening_end   = utc2localtime(observer.next_setting(sun, use_center=True).datetime(), tz)
  observer.horizon = '-18'
  try:
    astro_rise = utc2localtime(observer.next_rising(sun, use_center=True).datetime(), tz)
    astro_set = utc2localtime(observer.next_setting(sun, use_center=True).datetime(), tz)
  except ephem.AlwaysUpError:
    astro_rise="-"
    astro_set="-"
  observer.horizon = '0.0'

  #transit_angle = target.transit_alt
  transit_angle = rad2deg(transit_angle)
  distance = target.earth_distance * AU
  return({'astro_rising' : astro_rise,
          'blue_morning_begin': blue_morning_begin,
          'civil_rising' : civil_rise,
          'blue_morning_end': blue_morning_end,
          'rising'  : rising,
          'golden_morning_end' : golden_rise,
          'transit' : transit,
          'golden_evening_begin' : golden_set,
          'setting' : setting,
          'blue_evening_begin': blue_evening_begin,
          'civil_setting' : civil_set,
          'blue_evening_end': blue_evening_end,
          'astro_setting' : astro_set,
          'transit_angle' : transit_angle,
          'distance': distance})

tz       = timezone('Europe/Berlin')
date     = datetime.now(tz)
# date     = datetime.strptime("1899 December 31 12:00 UTC", '%Y %B %d %H:%M %Z')
# date     = datetime.strptime("2021 October 26 12:00 UTC", '%Y %B %d %H:%M %Z')
midnight = date.replace(minute=0, second=0, microsecond=0, hour=0)

location      = ephem.Observer()
## geht auch als 'dd:mm:ss'
location.lon  = '8.2764' 
location.lat  = '50.0065'
location.elev = 80.0
location.date = midnight

sun  = ephem.Sun()

print("<table>")
print("<tr><th>Name</th><th>Time</th></tr>")
x = rise_transit_set(location.copy(), sun.copy(), date)
if x['astro_rising'] == '-':
  time = "-"
else:
  time = "%02i:%02i" % (x['astro_rising'].hour, x['astro_rising'].minute)
print("<tr><td>Begin astronomical dawn</td><td>%s</td></tr>" %(time))
time = "%02i:%02i" % (x['blue_morning_begin'].hour, x['blue_morning_begin'].minute)
print("<tr><td>Begin blue hour</td><td>%s</td></tr>" %(time))
time = "%02i:%02i" % (x['civil_rising'].hour, x['civil_rising'].minute)
print("<tr><td>Begin civil dawn/golden hour</td><td>%s</td></tr>" %(time))
time = "%02i:%02i" % (x['blue_morning_end'].hour, x['blue_morning_end'].minute)
print("<tr><td>End blue hour</td><td>%s</td></tr>" %(time))
time = "%02i:%02i" % (x['rising'].hour, x['rising'].minute)
print("<tr><td>Sunrise</td><td>%s</td></tr>" %(time))
time = "%02i:%02i" % (x['golden_morning_end'].hour, x['golden_morning_end'].minute)
print("<tr><td>End golden hour</td><td>%s</td></tr>" %(time))
time = "%02i:%02i" % (x['transit'].hour, x['transit'].minute)
print("<tr><td>Culmination</td><td>%s</td></tr>" %(time))
time = "%02i:%02i" % (x['golden_evening_begin'].hour, x['golden_evening_begin'].minute)
print("<tr><td>Begin golden hour</td><td>%s</td></tr>" %(time))
time = "%02i:%02i" % (x['setting'].hour, x['setting'].minute)
print("<tr><td>Sunset</td><td>%s</td></tr>" %(time))
time = "%02i:%02i" % (x['blue_evening_begin'].hour, x['blue_evening_begin'].minute)
print("<tr><td>Begin blue hour</td><td>%s</td></tr>" %(time))
time = "%02i:%02i" % (x['civil_setting'].hour, x['civil_setting'].minute)
print("<tr><td>End civil dusk/golden hour</td><td>%s</td></tr>" %(time))
time = "%02i:%02i" % (x['blue_evening_end'].hour, x['blue_evening_end'].minute)
print("<tr><td>End blue hour</td><td>%s</td></tr>" %(time))
if x['astro_setting'] == '-':
  time ="-"
else:
  time = "%02i:%02i" % (x['astro_setting'].hour, x['astro_setting'].minute)
print("<tr><td>End astronomical dusk</td><td>%s</td></tr>" %(time))
print("</table>")
